package com.example.starterapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

@SuppressLint("ResourceAsColor")
public class DashboardFragment extends Fragment{
   // public static final String ARG_LAYOUT_RESOURCE = "layout";

	
    @SuppressWarnings("deprecation")
	public View onCreateView(LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.
    	
    	 View rootView;
     
        //Bundle args = getArguments();
        //int index=args.getInt(ARG_OBJECT);  
        rootView = inflater.inflate(
                R.layout.dashboard, container, false);
        rootView.setBackgroundColor(getResources().getColor(R.color.dbbackcolor));
        return rootView;
    }

}
