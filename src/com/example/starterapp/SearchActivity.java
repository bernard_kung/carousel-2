package com.example.starterapp;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class SearchActivity extends Activity {
	public static final String ARG_QUERY = "query";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		handleIntent(getIntent());
	}

	private void handleIntent(Intent intent) {
		// TODO Auto-generated method stub
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			// use the query to search your data somehow
			Log.d("handleIntent-query", query);
			try {
				Intent i = new Intent(this,Class.forName("com.example.starterapp.MainActivity"));
				intent.putExtra("viewname", query);
				startActivity(i);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

		}else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			Uri data = intent.getData();
			Log.d("data", data.getPath());
			Cursor c = getContentResolver().query(data, null, null, null, null);
			if (c == null) {
				Log.d("null", "no match");
				finish();
			} else {
				c.moveToFirst();
				int wIndex = c.getColumnIndexOrThrow(SearchDb.KEY_WORD);
				Log.d("feedback", c.getString(wIndex));
			}

		}

	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		handleIntent(intent);
	}

}
