package com.example.starterapp;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;

public class SearchDb {
	private static final String TAG = "SearchDatabase";

	// The columns we'll include in the suggestion table
	public static final String KEY_WORD = SearchManager.SUGGEST_COLUMN_TEXT_1;
	public static final String KEY_DESCRIPTION = SearchManager.SUGGEST_COLUMN_TEXT_2;

	private static final String DATABASE_NAME = "SearchSuggestionsDB";
	private static final String DATABASE_TABLE = "SuggestionsTable";
	private static final int DATABASE_VERSION = 3;

	private final Context suggestionsContext;
	private SuggestionsOpenHelper mSuggestionsOpenHelper;
	private static final HashMap<String,String> mColumnMap = buildColumnMap();
	
	/**
	 * Constructor
	 * 
	 * @param context
	 *            The Context within which to work, used to create the DB
	 */
	public SearchDb(Context c) {
		suggestionsContext = c;
		mSuggestionsOpenHelper = new SuggestionsOpenHelper(suggestionsContext);
	}
	
	/**
     * Builds a map for all columns that may be requested, which will be given to the 
     * SQLiteQueryBuilder. This is a good way to define aliases for column names, but must include 
     * all columns, even if the value is the key. This allows the ContentProvider to request
     * columns w/o the need to know real column names and create the alias itself.
     */
	private static HashMap<String,String> buildColumnMap() {
	        HashMap<String,String> map = new HashMap<String,String>();
	        map.put(KEY_WORD, KEY_WORD);
	        map.put(KEY_DESCRIPTION, KEY_DESCRIPTION);
	        map.put(BaseColumns._ID, "rowid AS " +
	                BaseColumns._ID);
	        map.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, "rowid AS " +
	                SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
	        map.put(SearchManager.SUGGEST_COLUMN_SHORTCUT_ID, "rowid AS " +
	                SearchManager.SUGGEST_COLUMN_SHORTCUT_ID);
	        return map;
	    }
	
    /**
     * Returns a Cursor positioned at the word specified by rowId
     *
     * @param rowId id of word to retrieve
     * @param columns The columns to include, if null then all are included
     * @return Cursor positioned to matching word, or null if not found.
     */
    public Cursor getWord(String rowId, String[] columns) {
        String selection = "rowid = ?";
        String[] selectionArgs = new String[] {rowId};
        Log.d("SELECT based on rowid",rowId);
        return query(selection, selectionArgs, columns);
        
        /* This builds a query that looks like:
         *     SELECT <columns> FROM <table> WHERE rowid = <rowId>
         */
    }

	 /**
     * Returns a Cursor over all words that match the given query
     *
     * @param query The string to search for
     * @param columns The columns to include, if null then all are included
     * @return Cursor over all words that match, or null if none found.
     */
	public Cursor getWordMatches(String query, String[] columns) {
        String selection = KEY_WORD + " MATCH ?";
        String[] selectionArgs = new String[] {query+"*"};
        
        //Log.d("selection",selection);
        //Log.d("selectionArgs",selectionArgs[0]);
        return query(selection, selectionArgs, columns);
	}
	  /**
     * Performs a database query.
     * @param selection The selection clause
     * @param selectionArgs Selection arguments for "?" components in the selection
     * @param columns The columns to return
     * @return A Cursor over all rows matching the query
     */
	private Cursor query(String selection, String[] selectionArgs, String[] columns) {
        /* The SQLiteBuilder provides a map for all possible columns requested to
         * actual columns in the database, creating a simple column alias mechanism
         * by which the ContentProvider does not need to know the real column names
         */
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(DATABASE_TABLE);
        builder.setProjectionMap(mColumnMap);

        Cursor cursor = builder.query(mSuggestionsOpenHelper.getReadableDatabase(),
                columns, selection, selectionArgs, null, null, null);

        if (cursor == null) {
        	Log.d("nothing","nothing");
            return null;
        } else if (!cursor.moveToFirst()) {
        	//Log.d("cursor close",""+cursor.getCount());
            cursor.close();
            return null;
        }
        return cursor;
    }
    /**
     * This creates/opens the database.
     */
	private static class SuggestionsOpenHelper extends SQLiteOpenHelper {
		private SQLiteDatabase suggestionsDatabase;
		private final Context mHelperContext;
		
        private static final String TABLE_CREATE =
                "CREATE VIRTUAL TABLE " + DATABASE_TABLE +
                " USING fts3 (" +
                KEY_WORD + ", " +
                KEY_DESCRIPTION + ");";
		
		public SuggestionsOpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			mHelperContext = context;
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			suggestionsDatabase = db;
			suggestionsDatabase.execSQL(TABLE_CREATE);
			loadSuggestions();
		}
		
        /**
         * Starts a thread to load the database table with words
         */
		 private void loadSuggestions() {
	            new Thread(new Runnable() {
	                @Override
					public void run() {
	                    try {
	                        loadWords();
	                    } catch (IOException e) {
	                        throw new RuntimeException(e);
	                    }
	                }
	            }).start();
	        }

		 private void loadWords() throws IOException {
	            Log.d(TAG, "Loading words...");
	            final Resources resources = mHelperContext.getResources();
	            InputStream inputStream = resources.openRawResource(R.raw.viewslibrary);
	            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

	            try {
	                String line;
	                while ((line = reader.readLine()) != null) {
	                    String[] strings = TextUtils.split(line, "-");
	                    if (strings.length < 2) continue;
	                    long id = addWord(strings[0].trim(), strings[1].trim());
	                    if (id < 0) {
	                        Log.e(TAG, "unable to add word: " + strings[0].trim());
	                    }
	                }
	            } finally {
	                reader.close();
	            }
	            Log.d(TAG, "DONE loading words.");
	        }
		 
		 public long addWord(String word, String definition) {
	            ContentValues initialValues = new ContentValues();
	            initialValues.put(KEY_WORD, word);
	            initialValues.put(KEY_DESCRIPTION, definition);

	            return suggestionsDatabase.insert(DATABASE_TABLE, null, initialValues);
	        }
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS" + DATABASE_TABLE);
			onCreate(db);
		}

		@Override
		public void onDowngrade(SQLiteDatabase db, int oldVersion,
				int newVersion) {
			// TODO Auto-generated method stub
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS" + DATABASE_TABLE);
			onCreate(db);
		}
		
		
	}


	
}
