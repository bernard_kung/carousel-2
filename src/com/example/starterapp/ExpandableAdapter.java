package com.example.starterapp;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ExpandableAdapter extends BaseExpandableListAdapter {
	private Activity context;
	private Map<String, List<String>> views;
	private List<String> viewGroups;
	
	public ExpandableAdapter(Activity context, List<String> viewGroups, Map<String,List<String>> views){
		this.context = context;
		this.viewGroups = viewGroups;
		this.views = views;
	}
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return views.get(viewGroups.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {
		// TODO Auto-generated method stub
		final String view = (String)getChild(groupPosition,childPosition);
		LayoutInflater inflater = context.getLayoutInflater();
		if(convertView ==null){
			convertView = inflater.inflate(R.layout.childview, null);	
		}
		TextView item = (TextView)convertView.findViewById(R.id.view);
		item.setText(view);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return views.get(viewGroups.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return viewGroups.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return viewGroups.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		String viewGroupName = (String) getGroup(groupPosition);
		if(convertView == null){
			LayoutInflater infalInfalter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInfalter.inflate(R.layout.viewgroup, null);
		}
		TextView item = (TextView)convertView.findViewById(R.id.viewgroup);
		item.setTypeface(null,Typeface.BOLD);
		item.setText(viewGroupName);
		//Log.d("viewGroupName",viewGroupName);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}

}
