package com.example.starterapp;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.atomic.AtomicInteger;

import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MotionEvent;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

public class MainActivity extends FragmentActivity {
	
	//Carousel
		private ImageView[] imageViews = null;
		private ImageView imageView = null;
		private ViewPager advPager = null;
		private AtomicInteger what = new AtomicInteger(0);
		private boolean isContinue = true;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;
	private SearchView searchView;

	// nav drawer var
	private String[] mViewGroupTitles;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mDrawerTitle;
	private CharSequence mTitle;

	public static final String ARG_QUERY = "query";

	private List<String> viewGroupList;
	private List<String> viewList;
	private Map<String, List<String>> viewCollection;
	private ExpandableListView mDrawerList;

	private String mSuggestion = "";

	private int count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initialPager();
		// nav drawer
		initialDrawer();
		initialActionBar();
	}
	
	//get rid bc of single page
	/*@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mViewPager.getCurrentItem() != 0) {
			mViewPager.setCurrentItem(0, true);
		} else {
			super.onBackPressed();
		}
	}*/

	private void handleIntent(Intent intent) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		Intent childIntent;
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			// use the query to search your data somehow
			try {
				childIntent = new Intent(this,
						Class.forName("com.example.starterapp.WebContent"));
				String url = "file:///android_asset/faqs/"
						+ query.replaceAll("\\s+", "").toLowerCase() + ".html";
				childIntent.putExtra("url", url);
				startActivity(childIntent);
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			Uri data = intent.getData();
			Log.d("data", data.getPath());
			Cursor c = getContentResolver().query(data, null, null, null, null);
			if (c == null) {
				Log.d("null", "no match");
				finish();
			} else {
				c.moveToFirst();
				int wIndex = c.getColumnIndexOrThrow(SearchDb.KEY_WORD);
				WebView wv = (WebView) mViewPager.findViewWithTag(0);
				try {
					childIntent = new Intent(this,
							Class.forName("com.example.starterapp.WebContent"));
					String url = "file:///android_asset/faqs/"
							+ c.getString(wIndex).replaceAll("\\s+", "")
									.toLowerCase() + ".html";
					childIntent.putExtra("url", url);
					startActivity(childIntent);
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_left);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.d("feedback", c.getString(wIndex));
			}

		}
		searchView.setIconified(true);

	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		handleIntent(intent);
	}

	private void initialPager() {
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(this);

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);

		mViewPager.setAdapter(mSectionsPagerAdapter);
		/*mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

					@Override
					public void onPageSelected(int position) {
						// TODO Auto-generated method stub
						getActionBar().setSelectedNavigationItem(position);
						//getActionBar().setTitle(mViewGroupTitles[position]);
					}

				}); */
	}

	private void initialDrawer() {
		mViewGroupTitles = getResources().getStringArray(R.array.viewgroups);
		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			@Override
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu();
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
		createViewGroupList();
		createViewCollection();
		mDrawerList = (ExpandableListView) findViewById(R.id.left_drawer);
		final ExpandableAdapter expListAdapter = new ExpandableAdapter(this,
				viewGroupList, viewCollection);
		mDrawerList.setAdapter(expListAdapter);
		mDrawerList.setGroupIndicator(this.getResources().getDrawable(
				R.drawable.setting_selector));
		// setGroupIndicatorToRight();

		mDrawerList.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				// TODO Auto-generated method stub
				final String selected = (String) expListAdapter.getChild(
						groupPosition, childPosition);
				Toast.makeText(getBaseContext(), selected, Toast.LENGTH_LONG)
						.show();
				Intent intent;
				if (groupPosition == 0) {
					try {
						intent = new Intent(v.getContext(), Class
								.forName("com.example.starterapp.WebContent"));
						String url = "file:///android_asset/faqs/"
								+ selected.replaceAll("\\s+", "").toLowerCase()
								+ ".html";
						intent.putExtra("url", url);
						startActivity(intent);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_left);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} //else if (groupPosition == 2) {
				else{
					try {
						intent = new Intent(
								v.getContext(),
								Class.forName("com.example.starterapp.ChildActivity"));
						startActivity(intent);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_left);
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				mDrawerList.setItemChecked(groupPosition, true);
				//setTitle(mViewGroupTitles[groupPosition]);
				mDrawerLayout.closeDrawer(mDrawerList);
				return true;
			}

		});

	}

	private void initialActionBar() {
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		//actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		

	}

	private void createViewCollection() {
		// TODO Auto-generated method stub
		String[] faqViews = { "Check List", "Emergency Info", "How do I" };
		String[] jargonBuster = { "JARGON BUSTER" };
		String[] otherViews = { "Building Map", "Video" };

		viewCollection = new LinkedHashMap<String, List<String>>();

		for (String vg : viewGroupList) {
			// Log.d("vg", vg);
			if (vg.equals("FAQ")) {
				loadChild(faqViews);
			} else if (vg.equals("OTHERS")) {
				loadChild(otherViews);
			} else if (vg.equals("JARGON BUSTER")) {
				loadChild(jargonBuster);
			}
			viewCollection.put(vg, viewList);
		}
	}

	private void createViewGroupList() {
		// TODO Auto-generated method stub
		viewGroupList = new ArrayList<String>();
		viewGroupList.add("FAQ");
		viewGroupList.add("JARGON BUSTER");
		viewGroupList.add("OTHERS");
	}

	private void loadChild(String[] views) {
		viewList = new ArrayList<String>();
		for (String view : views) {
			// Log.d("view", view);
			viewList.add(view);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.search).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * private class DrawerItemClickListener implements
	 * ListView.OnItemClickListener {
	 * 
	 * @Override public void onItemClick(AdapterView<?> parent, View view, int
	 * position, long id) { // TODO Auto-generated method stub
	 * selectItem(position); }
	 * 
	 * }
	 * 
	 * private void selectItem(int position) { // if (position != currentPos) {
	 * Log.d("Click nav drawer", "" + position);
	 * mViewPager.setCurrentItem(position); mDrawerList.setItemChecked(position,
	 * true); setTitle(mViewGroupTitles[position]);
	 * mDrawerLayout.closeDrawer(mDrawerList); }
	 */

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

		searchView = (SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		searchView.setQueryHint("Search Grad Starter App");
		searchView.setQueryRefinementEnabled(true);
		return true;
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends PagerAdapter {

		private Activity activity;

		public SectionsPagerAdapter(Activity act) {
			activity = act;
		}

		@SuppressLint("SetJavaScriptEnabled")
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// TODO Auto-generated method stub
			if (position == 0) {
				/*
				 * // Log.d("instantiateItem", "" + position); WebView wv = new
				 * WebView(activity);
				 * wv.getSettings().setJavaScriptEnabled(true);
				 * wv.getSettings().setLoadWithOverviewMode(true);
				 * wv.getSettings().setUseWideViewPort(true);
				 * wv.setWebChromeClient(new WebChromeClient());
				 * wv.setTag(position); try { String url =
				 * "file:///android_asset/faqs/firstconvos.html";;
				 * wv.loadUrl(url); } catch (Exception e) {
				 * Log.d("webview loading exception", e.getMessage()); }
				 * ((ViewPager) container).addView(wv, 0); return wv;
				 */
				View rootview;
				LayoutInflater inflater = (LayoutInflater) container
						.getContext().getSystemService(
								Context.LAYOUT_INFLATER_SERVICE);
				int resId = R.layout.dashboard2;
				rootview = inflater.inflate(resId, null);
				rootview.setBackgroundColor(getResources().getColor(
						R.color.dbbackcolor));
				

				
				advPager = (ViewPager) rootview.findViewById(R.id.adv_pager);

				List<View> advPics = new ArrayList<View>();

				
				ImageView img0 = new ImageView(getBaseContext());
				img0.setBackgroundResource(R.drawable.ad7contact_us);
				advPics.add(img0);
				
				
				ImageView img1 = new ImageView(getBaseContext());
				img1.setBackgroundResource(R.drawable.ad1my_first_day);
				img1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try {
							Intent intent = new Intent(
									v.getContext(),
									Class.forName("com.example.starterapp.WebContent"));
							String url = "file:///android_asset/faqs/"
									+ "howdoi".replaceAll("\\s+", "")
											.toLowerCase() + ".html";
							intent.putExtra("url", url);
							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_left);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});
				advPics.add(img1);

				ImageView img2 = new ImageView(getBaseContext());
				img2.setBackgroundResource(R.drawable.ad2jargon_buster);
				advPics.add(img2);

				ImageView img3 = new ImageView(getBaseContext());
				img3.setBackgroundResource(R.drawable.ad3essential_info);
				advPics.add(img3);

				ImageView img4 = new ImageView(getBaseContext());
				img4.setBackgroundResource(R.drawable.ad4_around_me);
				advPics.add(img4);
				
				ImageView img5 = new ImageView(getBaseContext());
				img5.setBackgroundResource(R.drawable.ad5itogether);
				advPics.add(img5);
				
				ImageView img6 = new ImageView(getBaseContext());
				img6.setBackgroundResource(R.drawable.ad6grad_network);
				advPics.add(img6);
				
				ImageView img7 = new ImageView(getBaseContext());
				img7.setBackgroundResource(R.drawable.ad7contact_us);
				advPics.add(img7);
				
				ImageView img8 = new ImageView(getBaseContext());
				img8.setBackgroundResource(R.drawable.ad1my_first_day);
				advPics.add(img8);
		  
				
				ViewGroup group = (ViewGroup) rootview.findViewById(R.id.viewGroup2);
				imageViews = new ImageView[advPics.size()];
				


				for (int i = 0; i < advPics.size(); i++) {
					imageView = new ImageView(getBaseContext());
					imageView.setLayoutParams(new LayoutParams(20, 20));
					imageView.setPadding(20, 0, 20, 0);
					imageViews[i] = imageView;
					if (i == 1) {
						imageViews[i]
								.setBackgroundResource(R.drawable.banner_dian_transparent);				
					} else if(i > 1 && i < advPics.size()-1){
						imageViews[i]
								.setBackgroundResource(R.drawable.banner_dian_transparent);
					}
					else
					{
						imageViews[i]
								.setBackgroundResource(R.drawable.banner_dian_transparent);
					}
					
					group.addView(imageViews[i]);
				}
				
				advPager.setAdapter(new AdvAdapter(advPics));
				advPager.setOnPageChangeListener(new GuidePageChangeListener());

				advPager.setOnTouchListener(new View.OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						switch (event.getAction()) {
						case MotionEvent.ACTION_DOWN:
						case MotionEvent.ACTION_MOVE:
							isContinue = false;
							break;
						case MotionEvent.ACTION_UP:
							isContinue = true;
							break;
						default:
							isContinue = true;
							break;
						}
						return false;
					}
				});

				what.set(1);
				new Thread(new Runnable() {

					@Override
					public void run() {
						while (true) {
							if (isContinue) {
								viewHandler.sendEmptyMessage(what.get());
								whatOption();
							}
						}
					}

				}).start();
				
				
				/*
				ImageButton howdoi = (ImageButton) rootview
						.findViewById(R.id.ibHdi);
				howdoi.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try {
							Intent intent = new Intent(
									v.getContext(),
									Class.forName("com.example.starterapp.WebContent"));
							String url = "file:///android_asset/faqs/"
									+ "howdoi".replaceAll("\\s+", "")
											.toLowerCase() + ".html";
							intent.putExtra("url", url);
							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_left);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});
				ImageButton mytask = (ImageButton) rootview
						.findViewById(R.id.ibMt);
				mytask.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try {
							Intent intent = new Intent(
									v.getContext(),
									Class.forName("com.example.starterapp.WebContent"));
							String url = "file:///android_asset/faqs/"
									+ "checklist".replaceAll("\\s+", "")
											.toLowerCase() + ".html";
							intent.putExtra("url", url);
							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_left);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});
				ImageButton jb = (ImageButton) rootview.findViewById(R.id.ibJb);
				jb.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						try {
							Intent intent = new Intent(
									v.getContext(),
									Class.forName("com.example.starterapp.ChildActivity"));
							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_left);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});

				ImageButton grad = (ImageButton) rootview
						.findViewById(R.id.ibGrad);
				grad.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						try {
							Intent intent = new Intent(
									v.getContext(),
									Class.forName("com.example.starterapp.ChildActivity"));
							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_left);
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});*/
				((ViewPager) container).addView(rootview, position);
				return rootview;
			} else {
				/*TextView tv = new TextView(activity);
				;
				tv.setText("" + position);
				((ViewPager) container).addView(tv, position);
				return tv;*/
				//removed bc of single page
				return null;
			}

		}

		@Override
		public int getCount() {
			// Show 1 total pages.
			return 1;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			/*case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l); */
			}
			return null;
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			// TODO Auto-generated method stub
			((ViewPager) container).removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			// TODO Auto-generated method stub
			return arg0 == ((View) arg1);
		}

	}
	
	
	//Carousel


		private void whatOption() {
			what.incrementAndGet();
			if (what.get() > imageViews.length - 1) {
				what.getAndAdd(-8);
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				
			}
		}


		@SuppressLint("HandlerLeak")
		private final Handler viewHandler = new Handler() {

			@Override
			public void handleMessage(Message msg) {
				advPager.setCurrentItem(msg.what);
				super.handleMessage(msg);
			}

		};

		private final class GuidePageChangeListener implements OnPageChangeListener {

			@Override
			public void onPageScrollStateChanged(int state) {
				if (state == advPager.SCROLL_STATE_IDLE) {
					int curr = advPager.getCurrentItem();
					int lastReal = advPager.getAdapter().getCount() - 2;
					if (curr == 0) {
						advPager.setCurrentItem(lastReal, false);
						what.set(lastReal);
					} else if (curr > lastReal) {
						advPager.setCurrentItem(1, false);
						what.set(1);
					}
					}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageSelected(int arg0) {
				for (int i = 0; i < imageViews.length; i++) {
					
					if(i!=0 && i!=imageViews.length-1)
					{
						imageViews[arg0]
								.setBackgroundResource(R.drawable.banner_dian_transparent);
						if (arg0 != i) {
							imageViews[i]
									.setBackgroundResource(R.drawable.banner_dian_transparent);
						}
					}
					else
					{
						imageViews[i]
								.setBackgroundResource(R.drawable.banner_dian_transparent);
					}
				}
				what.set(arg0);

			}

		}


		private final class AdvAdapter extends PagerAdapter {
			private List<View> mmviews = null;

			public AdvAdapter(List<View> views) {
				this.mmviews = views;
			}

			@Override
			public void destroyItem(View arg0, int arg1, Object arg2) {
				((ViewPager) arg0).removeView(mmviews.get(arg1));
			}

			@Override
			public void finishUpdate(View arg0) {

			}

			@Override
			public int getCount() {
				return mmviews.size();
			}

			@Override
			public Object instantiateItem(View arg0, int arg1) {
				((ViewPager) arg0).addView(mmviews.get(arg1), 0);
				return mmviews.get(arg1);
			}

			@Override
			public boolean isViewFromObject(View arg0, Object arg1) {
				return arg0 == arg1;
			}

			@Override
			public void restoreState(Parcelable arg0, ClassLoader arg1) {

			}

			@Override
			public Parcelable saveState() {
				return null;
			}

			@Override
			public void startUpdate(View arg0) {

			}

		}
	
	
}
